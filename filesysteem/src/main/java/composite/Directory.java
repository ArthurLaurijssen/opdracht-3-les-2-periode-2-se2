package composite;

import java.util.*;

public final class Directory implements Container {
  private final String name;
  private final List<Component> children;
  private Directory parent;

  public Directory(String name){
    this.name = name;
    this.parent = null;
    children = new ArrayList<Component>();
  }

  @Override
  public long getSize() {
    long size = 0;
    for (Component component : children) {
        size += component.getSize();
    }
    return size;
  }

  @Override
  public String getPath() {
    if (parent !=null) {
      return parent.getPath() + "/" + name;
    }
    return name;
  }

  @Override
  public void setParent(Directory parent) {
    this.parent = parent;

  }
  public void add(Component c) {
    this.children.add(c);
    c.setParent(this);
  }
  public void remove(Component c) {
    if (this.children.contains(c)) {
      this.children.remove(c);
    }
  }
  @Override
  public String toString() {
    return this.getPath() + "(" + this.getSize() + "kb)";
  }

}
